/**
 * 
 */
package userinterface;

import javax.swing.BoxLayout;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.io.File;

import javax.swing.*;

import controller.Controller;

/**
 * @author Las Osman
 * @version 20201023
 * @epost: lasosman@outlook.com
 */
public class ImportFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	private Controller controller;

	private File file;

	public ImportFrame(Controller controller) {
		this.controller = controller;
		add(importPanel());

		this.setSize(300, 300);
		this.setVisible(true);
	}


	private JPanel importPanel(){

		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

		JLabel fileLabel = new JLabel("File:");
		JButton chooseButton = new JButton("Choose file");
		JButton importButton = new JButton("Import");
		importButton.setEnabled(false); //Locks this button until a file is chosen
		JTextField textField = new JTextField();
		chooseButton.addActionListener(e -> {
			JFileChooser chooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter(
					"CSV files", "csv");
			chooser.setFileFilter(filter);
			int returnVal = chooser.showOpenDialog(null);
			if(returnVal == JFileChooser.APPROVE_OPTION) {
				file =  chooser.getSelectedFile();
				importButton.setEnabled(true);//Unlocks it
				fileLabel.setText("File: " + file.getName());
			}
		});
		importButton.addActionListener(e -> {
			String name;
			if (textField.getText().isEmpty()) {
				name = "no name";
			}
			else {
				name =  textField.getText();
			}
			controller.importCsvFile(file, name);
			controller.valuesUpdated();
			dispose();
		});
		

		panel.add(fileLabel);
		panel.add(chooseButton);
		panel.add(new JLabel("Activity name:"));


		panel.add(textField);
		panel.add(importButton);




		return panel;
	}
}
