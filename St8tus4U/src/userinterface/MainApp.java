package userinterface;


import controller.Controller;
import controller.LoginController;

/**
 * @author Las Osman
 * @version 20201003
 * @epost: lasosman@outlook.com
 */
public class MainApp {
	public static void main(String[] args) {
		
		
		Controller controller = new Controller();
		LoginController loginController = new LoginController(controller);
		javax.swing.SwingUtilities.invokeLater( () -> new LoginFrame(loginController, controller));
		/*
		controller.setCurrentUser(new User(0,"Guest"));
		controller.syncFromDatabase();
		javax.swing.SwingUtilities.invokeLater( () -> new MainFrame(controller));*/
	}
}
