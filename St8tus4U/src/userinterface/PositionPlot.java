/**
 * 
 */
package userinterface;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JPanel;

import model.ActivityType;
import model.TrackPointType;

/**
 * @author Hugo Persson
 * @version 20201027
 * @epost: hugopersson7@hotmail.com
 */
public class PositionPlot extends JPanel {
	private static final long serialVersionUID = 1L;
	ActivityType activity;
	List<TrackPointType> trackpoints;
	
	double minLat;
    double maxLat;
    double minLon;
    double maxLon;

	public PositionPlot(ActivityType activity) {
		this.activity =  activity;
		trackpoints = activity.getTrackPoints();
		
		setPreferredSize(new Dimension(300,5));
	}

	public void paintComponent(Graphics g){
        activity.getTrackPoints();

		super.paintComponent(g);
		findMaxMinLongLat();

		//Initialize arrays
		int[] xArray = new int[trackpoints.size()];
		int[] yArray = new int[trackpoints.size()];
        
		//Loop through all TrackPoints stored in the list 'trackpoints'
		int i=0;
		for (TrackPointType tp : trackpoints)
		{
			xArray[i] = getXPixValue(tp);
			yArray[i] = getYPixValue(tp);
			i++;
		}
		g.setColor(Color.BLUE);
		g.drawPolyline(xArray, yArray, xArray.length);
	}

	// Find min and max lon/lat to know the area to plot in
	private void findMaxMinLongLat(){
		//just to start with some value:
		minLat = trackpoints.get(0).getLatitude();
		minLon = trackpoints.get(0).getLongitude();
		maxLat = trackpoints.get(0).getLatitude();
		maxLon = trackpoints.get(0).getLongitude();
        
		//Finds min and max:
		for (TrackPointType tp : trackpoints){
			double lon = tp.getLongitude();
			double lat = tp.getLatitude();

			if (lon > maxLon)
				maxLon = lon;
			else if (lon < minLon)
				minLon = lon;
			if (lat > maxLat)
				maxLat = lat;
			else if (lat < minLat)
				minLat = lat;
		}
	}

	//Read Longitude value from TrackPoint tp and transform it to suitable pixel value in x-direction
	private int getXPixValue(TrackPointType tp) {
		int xPix = (int)(((tp.getLongitude() - minLon) / (maxLon - minLon)) * getWidth());
		return xPix;
	}
    
	//Read Latitude value from TrackPoint tp and transform it to suitable pixel value in y-direction
	private int getYPixValue(TrackPointType tp) {
		int yPix = (int)(((tp.getLatitude() - minLat) / (maxLat - minLat)) * getHeight());
        
		yPix = getHeight()-yPix; //To adjust for y-axis going "downwards" in graphics
		return yPix;
	}

}
