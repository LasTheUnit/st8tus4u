package userinterface;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import controller.Controller;
import controller.LoginController;


public class LoginFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	
	private LoginController loginController;
	private Controller controller;

	public LoginFrame(LoginController loginController, Controller controller)
	{
		this.controller = controller;
		this.loginController = loginController;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		add(loginPanel());
		
		setSize(300,150); // eller pack() om minimal
		setVisible(true);
	}
	
	public JPanel loginPanel() {
		JPanel loginPanel = new JPanel(null);
		JTextArea loginTextArea = new JTextArea("");
		JButton loginButton = new JButton("Login");
		JLabel loginLabel = new JLabel("Username:"); 
		JLabel failedMessageLabel = new JLabel();
		
	    loginButton.addActionListener(e ->{
	    	/*
		    User user = new User(loginTextArea.getText().trim());
		    
		    int usernameExists = 0;
			for(String listedUser : usernames){
				
				if(user.getUsername().compareTo(listedUser) == 0) {
					usernameExists++;
				}				
			} 			
			if(usernameExists == 0) {
				usernames.add(user.getUsername());
			}
			
			for(String listedUser : usernames){
				System.out.println(listedUser);
			}
			
			/*Byt ut TestMainFrame*/
	    	String userInput = loginTextArea.getText().trim();
	    	boolean succeded = loginController.loginUser(userInput);
	    	if (succeded) {
			    SwingUtilities.invokeLater( () -> new MainFrame(controller));
			    setVisible(false);		
			    dispose();
			}
	    	else {
				failedMessageLabel.setText("Failed to login");
			}
	    	
	    });
	    
		loginLabel.setBounds(10,10,100,20);
		loginTextArea.setBounds(90,10,100,20);
		loginButton.setBounds(40,60,100,20);
		failedMessageLabel.setBounds(10, 40, 100, 20);
		
	    loginPanel.add(loginLabel);
		loginPanel.add(loginTextArea);
		loginPanel.add(loginButton);
		loginPanel.add(failedMessageLabel);

		return loginPanel;
	}
}
