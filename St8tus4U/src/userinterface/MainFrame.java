/**
 * 
 */
package userinterface;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.*;

import controller.Controller;
import model.ActivityType;



/**
 * @author Las Osman
 * @version 20201002
 * @epost: lasosman@outlook.com
 */
public class MainFrame extends JFrame implements Observer{
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabbedPane = new JTabbedPane();
	private Controller controller;

	public MainFrame(Controller controller) {
		this.controller = controller;
		controller.addObserver(this);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); 

		//Meny bar
		setJMenuBar(new JMenuBar());
		getJMenuBar().add(fileMenu());


		//TabbedPane
		add(tabbedPane);
		tabbedPane.add("Overview", overviewPanel());
		tabbedPane.add("Graphs", graphPanel());
		tabbedPane.add("Activities", activitiesPanel());
		tabbedPane.add("User profile", userPanel());


		this.setSize(600, 600);
		this.setVisible(true);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		//There is a better solution for this, I realized it to late, this works. Not enough time to implement a better solution for this.
		tabbedPane.setComponentAt(0, overviewPanel());
		tabbedPane.setComponentAt(1, graphPanel());
		tabbedPane.setComponentAt(2, activitiesPanel());
		tabbedPane.setComponentAt(3, userPanel());
		
	}

	private JMenu fileMenu()
	{
		JMenu fileMenu = new JMenu("File");
		JMenuItem menuOpen = new JMenuItem("Import Activity");
		menuOpen.addActionListener(e-> {
			javax.swing.SwingUtilities.invokeLater( () -> {
				new ImportFrame(controller);
			});
		});


		JMenuItem menuExit = new JMenuItem("Exit");          
		menuExit.addActionListener(e->System.exit(0));

		fileMenu.add(menuOpen);
		fileMenu.add(menuExit);
		return fileMenu;
	}

	private JPanel overviewPanel() {
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		panel.add(new JLabel("Activity name: " + controller.getActivityName()));
		JPanel mapPanel = new JPanel(new GridLayout(1,1));
		if (controller.getCurrentActivity() != null) {
			PositionPlot plot = new PositionPlot(controller.getCurrentActivity());
			mapPanel.add(plot);
		}
		//Statistics section
		JPanel activityStatistics = new JPanel();
		activityStatistics.setLayout(new BoxLayout(activityStatistics, BoxLayout.PAGE_AXIS));

		activityStatistics.add(new JLabel(String.format("Total Time: %d", controller.activityTotalTime())));
		activityStatistics.add(new JLabel(String.format("Max Altitude: %.02f", controller.activityMinAltitude())));
		activityStatistics.add(new JLabel(String.format("Min Altitude: %.02f", controller.activityMaxAltitude())));
		activityStatistics.add(new JLabel(String.format("Total distance: %.02f", controller.activityTotalDistance())));
		activityStatistics.add(new JLabel(String.format("Max heart rate: %d", controller.activityMaxHeartRate())));
		activityStatistics.add(new JLabel(String.format("Min heart rate: %d", controller.activityMinHeartRate())));
		activityStatistics.add(new JLabel(String.format("Average heart rate: %d", controller.activityAverageHeartRate())));
		activityStatistics.add(new JLabel(String.format("Max speed: %.02f", controller.activityMaxSpeed())));
		activityStatistics.add(new JLabel(String.format("Average speed: %.02f", controller.activityAverageSpeed())));
		activityStatistics.add(new JLabel(String.format("Max Cadence: %.02f", controller.activityMaxCadence())));
		activityStatistics.add(new JLabel(String.format("Average speed: %.02f", controller.activityAverageSpeed())));

		panel.add(mapPanel);
		panel.add(activityStatistics);

		return panel;
	}

	private JPanel graphPanel() {
		JPanel panel = new JPanel(new GridLayout(4, 1));
		if (controller.getCurrentActivity() != null) {
			panel.add(new PlotView("HR", controller.getCurrentActivity(), tp -> tp.getHeartRate()));
			panel.add(new PlotView("Altitude", controller.getCurrentActivity(), tp -> tp.getAltitude()));
			panel.add(new PlotView("Speed", controller.getCurrentActivity(), tp -> tp.getSpeed()));
			panel.add(new PlotView("Cadence", controller.getCurrentActivity(), tp -> tp.getCadence()));
		}
		return panel;
	}

	private JPanel activitiesPanel() {
		JPanel panel = new JPanel(new GridLayout(2,1));

		//Rename activity panel
		JPanel renameActivityPanel = new JPanel();
		renameActivityPanel.setLayout(new FlowLayout());
		renameActivityPanel.add(new JLabel("Activity name: "));

		JTextField activityNameField = new JTextField(controller.getActivityName());
		activityNameField.setPreferredSize(new Dimension(200, 20));

		JButton setNameButton = new JButton("Set name");

		setNameButton.addActionListener(e -> {
			controller.setActivityName(activityNameField.getText());
			//javax.swing.SwingUtilities.invokeLater( () -> new MainFrame(controller));
			//this.dispose();
			//this.repaint();
		});

		renameActivityPanel.add(activityNameField);
		renameActivityPanel.add(setNameButton);

		//Select activity panel
		JPanel selectActivityPanel = new JPanel();
		selectActivityPanel.setLayout(new FlowLayout());
		selectActivityPanel.add(new JLabel("Activities"));

		List<Integer> activityIds = new ArrayList<Integer>();

		JComboBox<String> activityComboBox = new JComboBox<String>();
		activityComboBox.setPreferredSize(new Dimension(200, 20));

		for (ActivityType activity : controller.getUserActivities()) {
			activityComboBox.addItem(activity.toString());
			activityIds.add(activity.getActivityId());
		}

		JButton selectButton = new JButton("Select");
		selectButton.addActionListener(e -> {
			int index = activityComboBox.getSelectedIndex();
			if(index > -1) {
				int id = activityIds.get(index);
				controller.setCurrentActivity(id);
				//javax.swing.SwingUtilities.invokeLater( () -> new MainFrame(controller));
				//this.dispose();
				//this.repaint();
			}
		});

		selectActivityPanel.add(activityComboBox);
		selectActivityPanel.add(selectButton);

		panel.add(renameActivityPanel);
		panel.add(selectActivityPanel);

		return panel;
	}

	private JPanel userPanel() {
		JPanel panel = new JPanel(null);

		JTextArea usernameTextArea = new JTextArea(controller.getCurrentUser().getUsername());
		JTextArea ageTextArea = new JTextArea(String.valueOf(controller.getCurrentUser().getAge()));
		JTextArea weightTextArea = new JTextArea(String.valueOf(controller.getCurrentUser().getWeight()));
		JTextArea maxPulseTextArea = new JTextArea(String.valueOf(controller.getCurrentUser().getMaxPulse()));

		JLabel usernameLabel = new JLabel("Username: "); 
		JLabel ageLabel = new JLabel("Age (years): ");
		JLabel weightLabel = new JLabel("Weight (kg): "); 
		JLabel maxPulseLabel = new JLabel("Max Pulse (BPM): ");

		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(e ->{
			try{
				int age = Integer.parseInt(ageTextArea.getText().trim());
				int weight = Integer.parseInt(weightTextArea.getText().trim());
				int maxPulse = Integer.parseInt(maxPulseTextArea.getText().trim());
				
				controller.updateUserInformantion(age, maxPulse, weight);
			}
			catch(NumberFormatException notNumber){
				JOptionPane.showMessageDialog(null,"Only numbers allowed!");
			}		    	    
		});

		usernameTextArea.setEditable(false);

		usernameLabel.setBounds(0,0,100,60);
		usernameTextArea.setBounds(120,20,80,20);

		ageLabel.setBounds(0,60,100,60);
		ageTextArea.setBounds(120,80,50,20);

		weightLabel.setBounds(0,140,100,60);
		weightTextArea.setBounds(120,160,50,20);

		maxPulseLabel.setBounds(0,220,100,60);			
		maxPulseTextArea.setBounds(120,240,50,20);

		saveButton.setBounds(20,300,100,60);

		panel.add(usernameLabel);
		panel.add(usernameTextArea);

		panel.add(ageLabel);
		panel.add(ageTextArea);

		panel.add(weightLabel);
		panel.add(weightTextArea);

		panel.add(maxPulseLabel);
		panel.add(maxPulseTextArea);

		panel.add(saveButton);

		return panel;
	}
}