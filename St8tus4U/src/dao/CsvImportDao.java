/**
 * 
 */
package dao;

import java.io.File;
import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.TrackPoint;
import model.TrackPointType;

/**
 * @author Las Osman
 * @version 20201022
 * @epost: lasosman@outlook.com
 */
public class CsvImportDao {
	public static List<TrackPointType> importTrackPoints(File file) throws Exception {
		List<TrackPointType> list = null;
		
		String extension = "";
		String fileName = file.getName();
		
		int indexOfDot = fileName.lastIndexOf('.');
		if(indexOfDot > 0)
			extension = fileName.substring(indexOfDot + 1);
		
		if (extension.equals("csv")) {
			list = new ArrayList<TrackPointType>();
			Scanner in = new Scanner(file);
			
			//Removing first row, contains column names which is not wanted.
			if(in.hasNextLine())
				in.nextLine();
			
			int id = 0;
			while (in.hasNextLine()) {
				String row = in.nextLine();
				Scanner rowScan = new Scanner(row);
				rowScan.useDelimiter(";");
				List<String> values = new ArrayList<String>();
				;
				while (rowScan.hasNext()) {
					String value = rowScan.next();
					value = value.replaceAll(",", ".");
					values.add(value);
				}

				TrackPointType point = new TrackPoint(id,Date.valueOf(values.get(0)), Time.valueOf(values.get(1)),
						Integer.parseInt(values.get(2)), Double.parseDouble(values.get(3)),
						Double.parseDouble(values.get(4)), Double.parseDouble(values.get(5)),
						Double.parseDouble(values.get(6)), (int) Double.parseDouble(values.get(7)),
						Double.parseDouble(values.get(8)), (int) Double.parseDouble(values.get(9)));

				rowScan.close();
				list.add(point);
				id++;
			}
			in.close();
		}
		else {
			throw new Exception("Invalid File");
		}
		
		return list;
	}
}
