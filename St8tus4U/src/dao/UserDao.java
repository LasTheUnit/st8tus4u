/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import db.DbConnectionManager;
import model.User;
import model.UserType;

/**
 * @author Las Osman
 * @version 20201029
 * @epost: lasosman@outlook.com
 */
public class UserDao{


	DbConnectionManager dbConManagerSingleton = null;

	public UserDao() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}


	public UserType get(int id) {
		UserType user = null;
		try{
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT user_id, user_name, age, max_pulse, weight FROM application_users WHERE user_id=" + id);
			if(resultSet.next())
				user = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getInt(5));
			dbConManagerSingleton.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	public UserType get(String name) {
		UserType user = null;
		try{
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT user_id, user_name, age, max_pulse, weight FROM application_users WHERE user_name= '" + name + "'");
			if(resultSet.next())
				user = new User(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4), resultSet.getInt(5));
			dbConManagerSingleton.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	public void update(UserType user) {
		PreparedStatement preparedStatement = null;

		try {
			preparedStatement = dbConManagerSingleton.prepareStatement(String.format("UPDATE application_users SET age = %d, max_pulse = %d, weight = %d WHERE user_id = %d",
					user.getAge(),
					user.getMaxPulse(),
					user.getWeight(),
					user.getUserId()));
			preparedStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
