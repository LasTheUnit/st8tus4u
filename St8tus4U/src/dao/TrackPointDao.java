/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import db.DbConnectionManager;
import model.TrackPoint;
import model.TrackPointType;

/**
 * @author Las Osman
 * @version 20201028
 * @epost: lasosman@outlook.com
 */
public class TrackPointDao {

	DbConnectionManager dbConManagerSingleton = null;

	public TrackPointDao() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}

	public List<TrackPointType> getTrackByActivityId(int activityId) {
		List<TrackPointType> trackPoints = new ArrayList<TrackPointType>();
		try{
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT track_id, activity_id, altitude, longitude, latitude, distance, time, date, elapsed_time, heart_rate, speed, candence FROM track_points WHERE activity_id=" + activityId);

			while (resultSet.next()) {
				trackPoints.add(new TrackPoint(
						resultSet.getInt(1), //TrackId
						resultSet.getDate(8), //Date
						resultSet.getTime(7), //time
						resultSet.getInt(9), //elapsedTime
						resultSet.getDouble(4), //longitude
						resultSet.getDouble(5), //latitude
						resultSet.getDouble(3), //altitude
						resultSet.getDouble(6), //distance
						resultSet.getInt(10), //hearRate
						resultSet.getDouble(11), //speed
						resultSet.getInt(12) //cadence
						));
			}

			dbConManagerSingleton.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		return trackPoints;
	}

	public void save(TrackPointType t, int activityId) {

		PreparedStatement preparedStatement = null;

		try {

			preparedStatement = dbConManagerSingleton.prepareStatement(
					"INSERT INTO track_points " +
					"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			preparedStatement.setInt(1, t.getTrackId());
			preparedStatement.setInt(2, activityId);
			preparedStatement.setDouble(3, t.getAltitude());
			preparedStatement.setDouble(4, t.getLongitude());
			preparedStatement.setDouble(5, t.getLatitude());
			preparedStatement.setDouble(6, t.getDistance());
			preparedStatement.setTime(7, t.getTime());
			preparedStatement.setDate(8, t.getDate());
			preparedStatement.setInt(9, t.getElapsedTime());			
			preparedStatement.setInt(10, t.getHeartRate());
			preparedStatement.setDouble(11, t.getSpeed());
			preparedStatement.setInt(12, t.getCadence());
			preparedStatement.execute();
		}
		catch ( SQLException e) {
			e.printStackTrace();
		}
	}

	public void saveMultipleTracks(List<TrackPointType> list, int activityId) {
		PreparedStatement preparedStatement = null;
		
		String statement = "INSERT INTO track_points VALUES";
		
		
		for (TrackPointType t : list) {
			statement += String.format(Locale.US," (%d, %d, %f, %f, %f, %f, '%tT', '%tF', %d, %d, %f, %d),",
					t.getTrackId(),
					activityId,
					t.getAltitude(),
					t.getLongitude(),
					t.getLatitude(),
					t.getDistance(),
					t.getTime(),
					t.getDate(),
					t.getElapsedTime(),
					t.getHeartRate(),
					t.getSpeed(),
					t.getCadence());
		}
		
		statement = statement.substring(0, statement.length() -1);
		try {
			preparedStatement = dbConManagerSingleton.prepareStatement(statement);
			preparedStatement.execute();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}	
	}
}