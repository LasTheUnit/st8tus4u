/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import db.DbConnectionManager;
import model.Activity;
import model.ActivityType;
/**
 * @author Las Osman
 * @version 20201028
 * @epost: lasosman@outlook.com
 */
public class ActivityDao{

	DbConnectionManager dbConManagerSingleton = null;

	public ActivityDao() {
		dbConManagerSingleton = DbConnectionManager.getInstance();
	}

	public ActivityType get(int id, TrackPointDao trackPointDao) {
		ActivityType activity = null;
		try{
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT activity_id, user_id, name FROM students WHERE activity_id=" + id);
			if( !resultSet.next())
				throw new NoSuchElementException("The Activity with id " + id + " doesn't exist in database");
			else
				activity = new Activity(
						id,
						resultSet.getString(3),
						trackPointDao.getTrackByActivityId(id)
						);
			dbConManagerSingleton.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		return activity;
	}

	public List<ActivityType> getActivitiesByUserId(int userId, TrackPointDao trackPointDao) {
		ArrayList<ActivityType> list = new ArrayList<>();
		try{
			ResultSet resultSet = dbConManagerSingleton.excecuteQuery("SELECT activity_id, user_id, name FROM activities WHERE user_id=" + userId);
			while(resultSet.next()) {
				list.add(new Activity(
						resultSet.getInt(1),
						resultSet.getString(3),
						trackPointDao.getTrackByActivityId(resultSet.getInt(1))
						));
			}

			dbConManagerSingleton.close();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}

	public ActivityType save(ActivityType t, int userId) {
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		//int rowCount = 0;
		//boolean saveSucess = false;
		try {
			preparedStatement = dbConManagerSingleton.prepareStatement(
					"INSERT INTO activities(user_id, name, date) " +
					"VALUES (?, ?, ?) RETURNING activity_id;");
			preparedStatement.setInt(1, userId);
			preparedStatement.setString(2, t.getActivityName());
			preparedStatement.setDate(3, t.getActivityDate());
			preparedStatement.execute();
			resultSet = preparedStatement.getResultSet();
			resultSet.next();
			int generatedId = resultSet.getInt(1);
			ActivityType activity;
			activity = new Activity(generatedId, t.getActivityName(), t.getTrackPoints());
			return activity;
		}
		catch ( SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void updateName(ActivityType activity) {
		PreparedStatement preparedStatement = null;
		
		try {
			preparedStatement = dbConManagerSingleton.prepareStatement("UPDATE activities SET name = '" + activity.getActivityName() + "' WHERE activity_id =" + activity.getActivityId());
			preparedStatement.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
