/**
 * 
 */
package controller;
import dao.UserDao;
import model.UserType;

/**
 * @author Las Osman
 * @version 20201028
 * @epost: lasosman@outlook.com
 */
public class LoginController {
	
	Controller controller;
	UserDao userDao;
	public LoginController(Controller controller) {
		userDao = new UserDao();
		this.controller = controller;
	}
	
	public Boolean loginUser(String username) {
		UserType user = userDao.get(username);
		if(user != null) {
			controller.setCurrentUser(user);
			return true;
		}
		else {
			return false;
		}
		
	}
}
