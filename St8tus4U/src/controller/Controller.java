/**
 * 
 */
package controller;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Observable;

import dao.ActivityDao;
import dao.CsvImportDao;
import dao.TrackPointDao;
import dao.UserDao;
import model.Activity;
import model.ActivityType;
import model.TrackPointType;
import model.UserType;

/**
 * @author Las Osman
 * @version 20201022
 * @epost: lasosman@outlook.com
 */
public class Controller extends Observable {
	private ActivityType currentActivity = null;
	private UserType currentUser = null;

	private ActivityDao activityDao;
	private TrackPointDao trackPointDao;
	private UserDao userDao;
	
	public Controller() {
		activityDao = new ActivityDao();
		trackPointDao = new TrackPointDao();
		userDao = new UserDao();
	}

	public Collection<ActivityType> getUserActivities() {
		return currentUser.getActivities();
	}

	public ActivityType getCurrentActivity() {
		return currentActivity;
	}
	public void setCurrentActivity(ActivityType currentActivity) {
		this.currentActivity = currentActivity;
		valuesUpdated();
	}
	public void setCurrentActivity(int activityId) {
		this.currentActivity = currentUser.getActivity(activityId);
		valuesUpdated();
	}
	public UserType getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(UserType currentUser) {
		this.currentUser = currentUser;
	}
	
	public void syncFromDatabase() {
		if (currentUser.getActivities().size() > 0) {
			currentUser.removeAllActivity();
		}
		List<ActivityType> list = activityDao.getActivitiesByUserId(currentUser.getUserId(), trackPointDao);
		for (ActivityType activity : list) {
			currentUser.addActivity(activity);
		}
		valuesUpdated();
	}

	public void importCsvFile(File file, String name) {
		ActivityType activity;
		try {
			List<TrackPointType> list =  CsvImportDao.importTrackPoints(file);
			activity = new Activity(-1, name, list);
			ActivityType syncedActivity = activityDao.save(activity, currentUser.getUserId());

			Runnable runnable = () ->{
				trackPointDao.saveMultipleTracks(list, syncedActivity.getActivityId());
			};
			new Thread(runnable).start();
			currentUser.addActivity(syncedActivity);
			setCurrentActivity(syncedActivity);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setActivityName(String name) {
		if (name.isEmpty()) {
			name = "no name";
		}
		currentActivity.setActivityName(name);
		activityDao.updateName(currentActivity);
		valuesUpdated();
	}

	public String getActivityName(){
		String value = "";
		if(currentActivity != null) {
			value = currentActivity.getActivityName();
		}
		return value;
	}
	public int activityTotalTime() {
		int value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getTotalTime();
		}
		return value;
	}

	public double activityMinAltitude() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getMaxAltitude();
		}
		return value;
	}

	public double activityMaxAltitude() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getMinAltitude();
		}
		return value;
	}

	public double activityTotalDistance() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getTotalDistance();
		}
		return value;
	}

	public int activityMaxHeartRate() {
		int value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getMaxHeartRate();
		}
		return value;
	}

	public int activityMinHeartRate() {
		int value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getMinHeartRate();
		}
		return value;
	}

	public int activityAverageHeartRate() {
		int value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getAverageHeartRate();
		}
		return value;
	}

	public double activityAverageSpeed() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getAverageSpeed();
		}
		return value;
	}

	public double activityMaxSpeed() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getMaxSpeed();
		}
		return value;
	}

	public double activityAverageCadence() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getAverageCadence();
		}
		return value;
	}

	public double activityMaxCadence() {
		double value = 0;
		if(currentActivity != null) {
			value = currentActivity.getStatistics().getMaxCadence();
		}
		return value;
	}
	
	public void updateUserInformantion(int age, int maxPulse, int weight) {
		currentUser.setAge(age);  
		currentUser.setWeight(weight);  
		currentUser.setMaxPulse(maxPulse);
		
		userDao.update(currentUser);
	}

	public void valuesUpdated() {
		setChanged();
		notifyObservers();
	}
}
