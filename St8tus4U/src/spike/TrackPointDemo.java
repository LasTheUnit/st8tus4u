/**
 * 
 */
package spike;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.TrackPointType;

/**
 * @author Las Osman
 * @version 20201020
 * @epost: lasosman@outlook.com
 */
public class TrackPointDemo {
	public static void main(String[] args) {
		List<TrackPointType> list = new ArrayList<TrackPointType>();
		
		//Reading row by row and create object out of it
		try {
			Scanner in = new Scanner(new File("E:\\csv\\CSV_TestFile.csv"));
			while(in.hasNextLine()) {
				String row = in.nextLine();
				Scanner rowScan = new Scanner(row);
				rowScan.useDelimiter(";");
				List<String> values = new ArrayList<String>();;
				while(rowScan.hasNext()) {
					String value = rowScan.next();
					value = value.replaceAll(",", ".");
					values.add(value);
				}
				
				/*TrackPointType point = new TrackPoint(
						values.get(0), 
						values.get(1), 
						Integer.parseInt(values.get(2)),
						Double.parseDouble(values.get(3)),
						Double.parseDouble(values.get(4)),
						Double.parseDouble(values.get(5)),
						Double.parseDouble(values.get(6)),
						(int) Double.parseDouble(values.get(7)),
						Double.parseDouble(values.get(8)),
						(int) Double.parseDouble(values.get(9)));*/
				
				rowScan.close();
				//list.add(point);
			}
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Done reading");
		
		for (TrackPointType trackPointType : list) {
			System.out.println(trackPointType.getTime().toString());
		}
	}
}
