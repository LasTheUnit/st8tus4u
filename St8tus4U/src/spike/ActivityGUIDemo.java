/**
 * 
 */
package spike;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

/**
 * @author Las Osman
 * @version 20201022
 * @epost: lasosman@outlook.com
 */
public class ActivityGUIDemo extends JFrame {
	private static final long serialVersionUID = 1L;
	private JTabbedPane tabbedPane = new JTabbedPane();

	public ActivityGUIDemo() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE); 
		
		//TabbedPane
		add(tabbedPane);
		tabbedPane.add("Activity", null);
		
		
		this.setSize(600, 600);
		this.setVisible(true);
	}
	
	//private JPanel activity 
	
	public static void main(String[] args) {

		javax.swing.SwingUtilities.invokeLater( () -> new ActivityGUIDemo());
	}
}
