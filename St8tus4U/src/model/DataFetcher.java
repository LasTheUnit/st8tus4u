/**
 * 
 */
package model;

/**
 * @author Las Osman
 * @version 20201027
 * @epost: lasosman@outlook.com
 */
public interface DataFetcher {
	public double fetch(TrackPointType firstTp);
}
