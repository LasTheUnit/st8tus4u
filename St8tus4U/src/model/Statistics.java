/**
 * 
 */
package model;

/**
 * @author Las Osman
 * @version 20201021
 * @epost: lasosman@outlook.com
 */
public class Statistics implements StatisticsType {
	
	private ActivityType activity;
	public Statistics(ActivityType activity) {
		this.activity = activity;
	}

	@Override
	public int getTotalTime() {
		int totalTime = activity.getTrackPoints().get(activity.getTrackPoints().size() - 1).getElapsedTime();
		return totalTime;
	}
	@Override
	public double getMaxAltitude(){
		double max = 0.0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			double value =  trackPoint.getSpeed();
			if(value > max)
				max = value;
		}
		return max;
	}
	@Override
	public double getMinAltitude(){
		double max = 0.0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			double value =  trackPoint.getSpeed();
			if(value > max)
				max = value;
		}
		return max;
	}

	@Override
	public double getTotalDistance() {
		double totalDiscance = activity.getTrackPoints().get(activity.getTrackPoints().size() - 1).getDistance();
		return totalDiscance;
	}

	@Override
	public int getMaxHeartRate() {
		int max = 0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			int value =  trackPoint.getHeartRate();
			if(value > max)
				max = value;
		}
		return max;
	}

	@Override
	public int getMinHeartRate() {
		int min = Integer.MAX_VALUE;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			int value =  trackPoint.getHeartRate();
			if(value < min)
				min = value;
		}
		return min;
	}

	@Override
	public int getAverageHeartRate() {
		int total = 0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			total += trackPoint.getHeartRate();
		}
		int average = total / activity.getTrackPoints().size();
		return average;
	}

	@Override
	public double getAverageSpeed() {
		double total = 0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			total += trackPoint.getSpeed();
		}
		double average = total / activity.getTrackPoints().size();
		return average;
	}

	@Override
	public double getMaxSpeed() {
		double max = 0.0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			double value =  trackPoint.getSpeed();
			if(value > max)
				max = value;
		}
		return max;
	}

	@Override
	public double getAverageCadence() {
		int total = 0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			total += trackPoint.getCadence();
		}
		int average = total / activity.getTrackPoints().size();
		return average;
	}

	@Override
	public double getMaxCadence() {
		int max = 0;
		for (TrackPointType trackPoint : activity.getTrackPoints()) {
			int value =  trackPoint.getCadence();
			if(value > max)
				max = value;
		}
		return max;
	}

}
