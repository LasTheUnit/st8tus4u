package model;
import java.util.Collection;
import java.util.HashMap;

public class User implements UserType {
	private int userId;
	private String username = "";
	private int age = 0;
	private int maxPulse = 0;
	private int weight = 0;
	
	private HashMap<Integer,ActivityType> activities = new HashMap<Integer,ActivityType>();
	
	public User(int id,String username) {
		this.userId = id;
		this.username = username;
	}
	
	
	
	public User(int userId, String username, int age, int maxPulse, int weight) {
		this.userId = userId;
		this.username = username;
		this.age = age;
		this.maxPulse = maxPulse;
		this.weight = weight;
	}


	@Override
	public ActivityType getActivity(int id) {
		return activities.get(id);
	}
	
	@Override
	public Collection<ActivityType> getActivities() {
		// TODO Auto-generated method stub
		return activities.values();
	}

	@Override
	public void removeActivity(ActivityType activity) {
		activities.remove(activity.getActivityId());
	}
	
	@Override
	public void removeAllActivity() {
		activities.clear();
	}

	@Override
	public void addActivity(ActivityType activity) {
		activities.put(activity.getActivityId(), activity);
	}
	
	@Override
	public int getUserId() {
		return userId;
	}
	
	@Override
	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setUsername(String username) {
		// TODO Auto-generated method stub
		this.username = username;
	}

	@Override
	public int getAge() {
		return age;
	}

	@Override
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int getMaxPulse() {
		return maxPulse;
	}

	@Override
	public void setMaxPulse(int maxPulse) {
		this.maxPulse = maxPulse;
	}

	@Override
	public int getWeight() {
		return weight;
	}

	@Override
	public void setWeight(int weight) {
		this.weight = weight;
	}
}
