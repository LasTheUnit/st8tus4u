/**
 * 
 */
package model;

import java.sql.Date;
import java.util.List;

/**
 * @author Las Osman
 * @version 20201020
 * @epost: lasosman@outlook.com
 */
public interface ActivityType {
	int getActivityId();
	void setActivityId(int activityId);
	String getActivityName();
	void setActivityName(String name);
	Date getActivityDate();
	Statistics getStatistics();
	List<TrackPointType> getTrackPoints();
}
