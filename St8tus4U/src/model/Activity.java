/**
 * 
 */
package model;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Las Osman
 * @version 20201020
 * @epost: lasosman@outlook.com
 */
public class Activity implements ActivityType {
	private int activityId;
	private String activityName;
	private Date activityDate;
	private Statistics statistics;

	private List<TrackPointType> trackPointlist = new ArrayList<TrackPointType>();
	
	public Activity(int id, String activityName, List<TrackPointType> trackPointlist) {
		this.activityId = id;
		this.activityName = activityName;
		this.trackPointlist = trackPointlist;
		this.activityDate = trackPointlist.get(0).getDate();
		this.statistics = new Statistics(this);
		
	}
	@Override
	public int getActivityId() {
		return activityId;
	}
	@Override
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	@Override
	public String getActivityName() {
		return activityName;
	}

	@Override
	public void setActivityName(String name) {
		this.activityName = name;
	}
	
	@Override
	public Date getActivityDate() {
		return activityDate;
	}

	@Override
	public List<TrackPointType> getTrackPoints() {
		return trackPointlist;
	}

	@Override
	public Statistics getStatistics() {
		return statistics;
	}
	
	@Override
	public String toString() {
		return activityName + " " + activityDate.toString();
	}
}
