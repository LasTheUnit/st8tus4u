/**
 * 
 */
package model;

import java.sql.Date;
import java.sql.Time;

/**
 * @author Las Osman
 * @version 20201020
 * @epost: lasosman@outlook.com
 */
public class TrackPoint implements TrackPointType{
	private int trackId;
	private Date date;
	private Time time;
	private int elapsedTime;
	private double longitude; 
    private double latitude;
    private double altitude; 
    private double distance;
	private int heartRate;
	private double speed;
	private int cadence;
	
	
	
	public TrackPoint(int trackId, Date date, Time time, int elapsedTime, double longitude, double latitude, double altitude,
			double distance, int heartRate, double speed, int cadence) {
		
		this.trackId = trackId;
		this.date = date;
		this.time = time;
		this.elapsedTime = elapsedTime;
		this.longitude = longitude;
		this.latitude = latitude;
		this.altitude = altitude;
		this.distance = distance;
		this.heartRate = heartRate;
		this.speed = speed;
		this.cadence = cadence;
	}


	@Override
	public int getTrackId() {
		return trackId;
	}
	
	@Override
	public Date getDate() {
		return date;
	}
	@Override
	public Time getTime() {
		return time;
	}
	@Override
	public int getElapsedTime() {
		return elapsedTime;
	}
	@Override
	public double getLongitude() {
		return longitude;
	}
	@Override
	public double getLatitude() {
		return latitude;
	}
	@Override
	public double getAltitude() {
		return altitude;
	}
	@Override
	public double getDistance() {
		return distance;
	}
	@Override
	public int getHeartRate() {
		return heartRate;
	}
	@Override
	public double getSpeed() {
		return speed;
	}
	@Override
	public int getCadence() {
		return cadence;
	}
}
