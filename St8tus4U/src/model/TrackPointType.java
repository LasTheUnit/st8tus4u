/**
 * 
 */
package model;

import java.sql.Time;
import java.sql.Date;

/**
 * @author Las Osman
 * @version 20201020
 * @epost: lasosman@outlook.com
 */
public interface TrackPointType {
	Date getDate();
	Time getTime();
	int getElapsedTime();
	double getLongitude();
	double getLatitude();
	double getAltitude();
	double getDistance();
	int getHeartRate();
	double getSpeed();
	int getCadence();
	int getTrackId();
}
