/**
 * 
 */
package model;

/**
 * @author Las Osman
 * @version 20201021
 * @epost: lasosman@outlook.com
 */
public interface StatisticsType {
	 int getTotalTime();
	 double getTotalDistance();
	double getMaxAltitude();
	double getMinAltitude();
	 int getMaxHeartRate();
	 int getMinHeartRate();
	 int getAverageHeartRate();
	 double getAverageSpeed();
	 double getMaxSpeed();
	 double getAverageCadence();
	 double getMaxCadence();
}
