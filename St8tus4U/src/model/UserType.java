/**
 * 
 */
package model;
import java.util.Collection;

/**
 * @author Las Osman
 * @version 20201023
 * @epost: lasosman@outlook.com
 */
public interface UserType {
	String getUsername();
	void setUsername(String username);
	int getAge();
	void setAge(int age);
	int getWeight();
	void setWeight(int weight);
	int getMaxPulse();
	void setMaxPulse(int maxPulse);
	int getUserId();
	void setUserId(int id);
	Collection<ActivityType> getActivities();
	void addActivity(ActivityType activity);
	void removeActivity(ActivityType activity);
	public ActivityType getActivity(int activityId);
	void removeAllActivity();
}
